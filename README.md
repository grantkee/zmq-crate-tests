# About
This binary has two competing crates for zeromq. Choose the crate you want by passing `tmq` for the [tmq](https://github.com/cetra3/tmq) crate or `zmq` for the [zmq](https://github.com/zeromq/zmq.rs) crate. The default is `zmq` if no arg is passed.

## How to run
#### ZMQ (or pass no args)
```bash
$ cargo run

or

$ cargo run -- zmq
```

#### TMQ
```bash
$ cargo run -- tmq
```