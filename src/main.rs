use zeromq::{ReqSocket, Socket, ZmqError};
use tmq::{request, Context, Message, TmqError};
use thiserror::Error;

#[tokio::main]
/// Main contains calls to two different crates that implement
/// zeromq. Run one or the other by passing `zmq` or `tmq` into
/// cargo run.
async fn main() -> std::result::Result<(), TestErrors> {
    let endpoint = "tcp://127.0.0.1:5555";
    let option: Vec<String> = std::env::args().collect();
    println!("matching on option: {:?}", option);
    match option.last() {
         Some(o) => {
            match o.as_str() {
                "tmq" => {
                    println!("calling run_tmq()...");
                    run_tmq(endpoint).await?;
                }
                _ => {
                    println!("calling run_zeromq()...");
                    run_zeromq(endpoint).await?;
                }
            }
        }
        None => {
            println!("calling run_zeromq()...");
            run_zeromq(endpoint).await?;
        }
    }

    Ok(())
}

/// Use the zeromq crate
async fn run_zeromq(endpoint: &str) -> std::result::Result<(), TestErrors> {
    let mut socket = ReqSocket::new();
    println!("new ReqSocket");
    let monitor = socket.monitor();
    let end = socket.bind(endpoint).await?;
    println!("bind to socket complete...spawning task");


    tokio::task::spawn(async move {
        println!("inside task::spawn()");
        let mut socket2 = ReqSocket::new();
        socket2
            .connect("tcp://127.0.0.1:5555")
            .await.expect("failed to connect");
        println!("socket successfully connected :D");
    });
    
    Ok(())
}

/// Use the tmq crate
async fn run_tmq(endpoint: &str) -> std::result::Result<(), TestErrors> {
    let mut send_socket = request(&Context::new()).connect(endpoint)?;
    let mut i = 0u32;

    loop {
        let message = format!("Req#{}", i);
        i += 1;

        println!("Request: {:?}", &message);
        let message: Message = message.as_bytes().into();
        let recv_sock = send_socket.send(message.into()).await?;
        let (msg, send) = recv_sock.recv().await?;
        send_socket = send;
        println!(
            "Reply: {:?}",
            msg.iter()
                .map(|item| item.as_str().unwrap_or("invalid text"))
                .collect::<Vec<&str>>()
        );

        if i > 10 {
            break
        }
    }

    Ok(())
}

#[derive(Error, Debug)]
enum TestErrors {
    #[error("Error with zeromq crate: {0}")]
    ZmqError(#[from] ZmqError),

    #[error("Error with tmq crate: {0}")]
    TmqError(#[from] TmqError),
}